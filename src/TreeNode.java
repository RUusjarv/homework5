import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class TreeNode {
	// http://enos.itcollege.ee/~jpoial/algoritmid/TreeNode.java
	// http://enos.itcollege.ee/~jpoial/algoritmid/puud.html
	// https://bitbucket.org/ihinno/homework5/
	private String name;
	private TreeNode firstChild;
	private TreeNode nextSibling;

	// Konstruktor
	TreeNode(String n, TreeNode d, TreeNode r) {
		name = n;
		firstChild = d;
		nextSibling = r;
	}

	TreeNode(String n) {
		this.name = n;
		this.firstChild = null;
		this.nextSibling = null;
	}

	public static void treeTest(String s) {
		// kontrollin stringi kõik vead, mis saab kohe teha.
		s = s.trim();
		int a = 0;
		int b = 0;
		// kas tühi
		if (s.isEmpty()) {
			throw new RuntimeException("Sisend: " + s + " on tühi, ei saa midagi teha");
		}
		// kas tühik vahel
		if (s.contains(" ")) {
			throw new RuntimeException("Sisend: " + s + " vahel on space");
		}
		// kas on tühi alampuu
		if (s.contains("()")) {
			throw new RuntimeException("Sisend: " + s + " on tühi alampuu");
		}
		// kas on topelt koma
		if (s.contains(",,")) {
			throw new RuntimeException("Sisend: " + s + " on topelt koma");
		}
		// kas on őige sulgude järjekord '' on chariga vőrdlemiseks ja ""
		// stringiga vőrdlemiseks
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '(') {
				break;
			} else if (s.charAt(i) == ')') {
				throw new RuntimeException("Sisend:" + s + " on vale sulgude järjekord");
			}
		}
		// countin sulud
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '(') {
				a++;
			}
			if (s.charAt(i) == ')') {
				b++;
			}
		}
		// kas sulge on võrdselt
		if (a != b) {
			throw new RuntimeException("Sisend: " + s + " pole mõlemaid sulge võrdselt");
		}
		// kas on, kuigi sulge pole
		if (s.contains(",") && a == 0) {
			throw new RuntimeException("Sisend: " + s + " on koma enne sulgu");
		}
		// kas sulg on õiges kohas olemas
		if (a > 0 && s.charAt(s.length() - 1) != ')') {
			throw new RuntimeException("Sisend: " + s + " viimane pole: )");
		}
	}

	public static TreeNode parsePrefix(String s) {
		treeTest(s);
		LinkedList<TreeNode> list = new LinkedList<TreeNode>();
		StringTokenizer tokenizer = new StringTokenizer(s, "(,)", true);
		String token = tokenizer.nextToken();
		TreeNode uus;
		TreeNode root = new TreeNode(token);
		list.add(root);
		int tase = 1;
		while (tokenizer.hasMoreTokens()) {
			token = tokenizer.nextToken();
			if (token.equals("(")) {
				token = tokenizer.nextToken();
				if (token.equals("("))
					throw new RuntimeException("Sisend: " + s + " on valel kohal: (");
				if (token.equals(","))
					throw new RuntimeException("Sisend: " + s + " on valel kohal: ,");
				if (token.equals(")"))
					throw new RuntimeException("Sisend: " + s + " on valel kohal: )");
				else
					uus = new TreeNode(token);
				tase = tase + 1;
				list.getLast().firstChild = uus;
				list.add(uus);
			}
			if (token.equals(")")) {
				tase = tase - 1;
				if (list.get(list.size() - 2).nextSibling == null)
					list.removeLast();
				else
					while (list.size() > tase)
						list.removeLast();

			} else if (token.equals(",")) {
				if (list.size() < 2)
					throw new RuntimeException("Sisend: " + s + " on valel kohal: ,");
				else
					token = tokenizer.nextToken();
				uus = new TreeNode(token);
				list.getLast().nextSibling = uus;
				list.add(uus);
			}

		}
		return root;
	}

	public String rightParentheticRepresentation() {
		StringBuilder b = new StringBuilder();
		TreeNode current = this.firstChild;
		if (current != null)
			b.append("(");
		while (current != null) {
			b.append(current.rightParentheticRepresentation());
			current = current.nextSibling;
			if (current != null)
				b.append(",");
		}
		if (this.firstChild != null)
			b.append(")");
		b.append(name);
		return b.toString();
	}

	public static void main(String[] param) {
		String s = "A(B1,C,D)";
		TreeNode t = TreeNode.parsePrefix(s);
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B1,C,D) ==> (B1,C,D)A
	}
}
